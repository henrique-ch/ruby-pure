users = [
  {username: "jonh", password:"12345"},
  {username: "Wick", password:"123456"},
  {username: "robert", password:"1234567"},
  {username: "senna", password:"12345678"},
  {username: "josh", password:"1234567"},
]


def auth_user(username, password, list_of_users)
  list_of_users.each do |user_row|
      if user_row[:username] == username && user_row[:password] == password
        return user_row 
      end
   end
  "Credentions were not correct"
end

puts "Welcome to the authenticator"
25.times { print "-" }
puts
puts "This progran will take input from the user and compare password"
puts "If password is correct, you will get back the user object"

attempts = 1

while attempts < 5
  print "Username: "
  username = gets.chomp
  print "Password: "
  password = gets.chomp
  authentication = auth_user(username, password, users)
  puts authentication
  puts "Press n to quit or any other key to continue: "
  input = gets.chomp.downcase
  break if input == "n"
  attempts += 1
end

puts "You have exceeded the number of attempts" if attempts == 5
